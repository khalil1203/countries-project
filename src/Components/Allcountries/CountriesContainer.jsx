import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { DarkThemeContext } from "../../DarkThemeContext";
import "./CountriesContainer.css";

const CountriesContainer = ({ sortedData }) => {
  const { dark } = useContext(DarkThemeContext);
  return (
    <div  className={(dark && "container cstmblack") || "container"}>
      {sortedData.map((entry, index) => {
        return (
          <Link to={`/country/${entry.cca3}`} key={index}>
            <div className="card" key={index}>
              <div className="imageContainer">
                <img src={entry.flags.png} alt="" />
              </div>

              <div className={(dark && "cardDetails black") || "cardDetails"}>
                <h2>{entry.name.common}</h2>
                <div>
                  <b>Population: </b>
                  {entry.population}
                </div>
                <div>
                  <b>Region: </b>
                  {entry.region}
                </div>
                <div>
                  <b>Capital: </b>
                  {entry.capital}
                </div>
              </div>
            </div>
          </Link>
        );
      })}
    </div>
  );
};

export default CountriesContainer;
