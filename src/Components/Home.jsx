import React from "react";
import Header from "./Header/Header";
import CountriesContainer from "./Allcountries/CountriesContainer.jsx";
import Searchbar from "./SearchBar/Searchbar";
import Loader from "./Loading/Loader";
import NotFound from "./Error/NotFound";

const Home = ({
  custom,
  countries,
  setCustom,
  currentData,
  setCurrentData,
  sortedData,
  setSortedData,
  loader
}) => {

  return (
    <>
      <Header />
      <Searchbar
        custom={custom}
        countries={countries}
        setCustom={setCustom}
        currentData={currentData}
        setCurrentData={setCurrentData}
        sortedData={sortedData}
        setSortedData={setSortedData}
      />
      {custom.length==0 && !loader ? <NotFound/> : <div/>}
      {loader  ? <Loader/> :<CountriesContainer sortedData={sortedData}/>}
    </>
  );
};

export default Home;
