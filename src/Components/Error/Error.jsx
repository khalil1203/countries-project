import React from "react";

const Error = () => {
  return (
    <div>
      <h1>Cannot retrieve data at the moment.</h1>
      <h2>Please try again later.</h2>
    </div>
  );
};

export default Error;
