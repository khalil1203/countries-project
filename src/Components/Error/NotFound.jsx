import React, { useContext } from 'react'
import { DarkThemeContext } from '../../DarkThemeContext';
import './Error.css';

const NotFound = () => {
  const {dark} = useContext(DarkThemeContext);
  return (
    <div className={dark && 'cstmblack' || ''} >
      <h1>Cannot find anything matching your search!!!</h1>
      <h2>Please search again!!!</h2>

    </div>
  )
}

export default NotFound
