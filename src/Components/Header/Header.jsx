import { useContext } from 'react'
import { DarkThemeContext } from '../../DarkThemeContext'
import ColorButton from '../ColorModeButton/ColorButton';
import './Header.css'
const Header = () => {
    const {dark} = useContext(DarkThemeContext);

  return (
    <div className={dark && 'header black' || 'header'}>
      <h2>Where in the World!</h2>
      <ColorButton/>
    </div>
  )
}

export default Header
