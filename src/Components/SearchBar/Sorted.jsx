import React, { useContext } from 'react'
import { DarkThemeContext } from '../../DarkThemeContext';

const Sorted = ({sortContent}) => {
  const {dark} = useContext(DarkThemeContext)
  return (
    <div>
      <select
      className= {dark && 'black' || ''}
          onChange={(e) => {
            sortContent(e.target.value);
          }}
        >
          <option >Sort</option>
          <option key="descendingPopulation" value="descendingPopulation">
            Descending Sort(Population)
          </option>
          <option key="ascendingPopulation" value="ascendingPopulation">
            Ascending Sort(Population)
          </option>
          <option key="descendingArea" value="descendingArea">Descending Sort(Area)</option>
          <option key="ascendingArea" value="ascendingArea">Ascending Sort(Area)</option>
        </select>

    </div>
  )
}

export default Sorted
