import React, { useContext } from 'react'
import { DarkThemeContext } from '../../DarkThemeContext';

const SubRegion = ({secondSelectFunc,subRegion,secondSelect}) => {
  const {dark} = useContext(DarkThemeContext);
  return (
      <div className="gap">
        {secondSelect && (
          <select
          className={dark && 'black' || ''}
            onChange={(e) => {
              secondSelectFunc(e.target.value);
            }}
          >
            <option key="select" >Filter By SubRegion</option>
            {Object.values(subRegion).map((entries) => (
              <option key={entries} value={entries}>{entries}</option>
            ))}
          </select>
        )}

    </div>
  )
}

export default SubRegion
