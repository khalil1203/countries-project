import React, { useContext } from 'react'
import { DarkThemeContext } from '../../DarkThemeContext';

const Search = ({searchFunc}) => {
  const {dark} = useContext(DarkThemeContext);
  return (
    <div >
      <input
        className= {dark && 'inputag black' || 'inputag'}
        type="text"
        onChange={(e) => {
          searchFunc(e.target.value);
        }
      }
        placeholder="Search For A Country..."
      />

    </div>
  )
}

export default Search
