import React, { useState, useEffect, useContext } from "react";
import Search from "./Search";
import SubRegion from "./SubRegion";
import Sorted from "./Sorted";
import Region from "./Region";
import './Search.css'
import { DarkThemeContext } from "../../DarkThemeContext";

const Searchbar = ({
  custom,
  countries,
  setCustom,
  currentData,
  setCurrentData,
  sortedData,
  setSortedData,
}) => {
  const [subRegion, setSubRegion] = useState({});
  const [secondSelect, setSecondSelect] = useState(false);
  const [region, setRegion] = useState("");
  const [sorted, setSorted] = useState("");
  const {dark} = useContext(DarkThemeContext);

  function selectChoose(e) {
    if (e == "all") {
      setSecondSelect(false);
    } else {
      setSecondSelect(true);
    }
  }

  function changeState(data) {
    let regionData;
    document.querySelector(".inputag").value = "";

    if (data == "all") {
      setCustom(countries);
      setCurrentData(countries);
      setSecondSelect(false);
    } else {
      regionData = countries
        .filter((entry) => {
          return entry.region.toLowerCase() == data.toLowerCase();
        })
        .map((entry) => entry);
      setRegion(data);
      setCustom(regionData);
      setCurrentData(regionData);
    }
  }

  function sortContent(e) {
    setSorted(e);
  }

  function searchFunc(e) {
    console.log(custom)
    let temp = currentData
      .filter((entry) => {
        let str1 = entry.name.common.toLowerCase();
        let str2 = e.toLowerCase();
        return str1.includes(str2);
      })
      .map((entry) => {
        return entry;
      });

    setCustom(temp);
  }

  function secondSelectFunc(e) {
    let temp = countries
      .filter((entry) => {
        let str1 = entry.subregion;
        let str2 = e;
        return str1 == str2;
      })
      .map((entry) => {
        return entry;
      });

    setCustom(temp);
    setCurrentData(temp);
  }

  useEffect(() => {
    let temp = countries
      .filter((entry) => {
        return entry.region == region;
      })
      .reduce((acc, entry) => {
        if (!acc[entry.subregion]) {
          acc[entry.subregion] = 0;
        }
        acc[entry.subregion] = entry.subregion;
        return acc;
      }, {});
    setSubRegion(temp);
  }, [custom]);

  useEffect(() => {
    let sortedResult = [...custom];

    if (sorted === "descendingPopulation") {
      sortedResult.sort((prev, cur) => cur.population - prev.population);
    } else if (sorted === "ascendingPopulation") {
      sortedResult.sort((prev, cur) => prev.population - cur.population);
    } else if (sorted === "descendingArea") {
      sortedResult.sort((prev, cur) => cur.area - prev.area);
    } else if (sorted === "ascendingArea") {
      sortedResult.sort((prev, cur) => prev.area - cur.area);
    }

    setSortedData(sortedResult);
  }, [custom, sorted]);

  return (
    <div className={dark && 'searchBox cstmblack' || 'searchBox'}>
      <Search searchFunc={searchFunc} />
      <SubRegion
        subRegion={subRegion}
        secondSelect={secondSelect}
        secondSelectFunc={secondSelectFunc}
      />
      <Sorted sortContent={sortContent} />
      <Region changeState={changeState} selectChoose={selectChoose} />
    </div>
  );
};

export default Searchbar;
