import React, { useContext } from 'react'
import { DarkThemeContext } from '../../DarkThemeContext';

const Region = ({changeState,selectChoose}) => {
  const {dark} = useContext(DarkThemeContext);
  return (
    <div>
      <select
      className= {dark && 'black' || ''}
          onChange={(e) => {
            changeState(e.target.value);
            selectChoose(e.target.value);
          }}
        >
          <option value="all">Filter By Region</option>
          <option value="Africa">Africa</option>
          <option value="Americas">America</option>
          <option value="Asia">Asia</option>
          <option value="Europe">Europe</option>
          <option value="Oceania">Oceania</option>
          <option value="Antarctic">Antarctica</option>
        </select>

    </div>
  )
}

export default Region
