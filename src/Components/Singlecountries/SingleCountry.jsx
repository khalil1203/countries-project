import React, { useContext} from "react";
import { Link,useParams } from "react-router-dom";
import { DarkThemeContext } from "../../DarkThemeContext";
import Loader from "../Loading/Loader";
import BorderCountries from "./BorderCountries";
import './SingleCountry.css'

const SingleCountry = ({countries}) => {

  const { dark } = useContext(DarkThemeContext);
  const id = useParams().id;
  let temp = countries.filter((entry) => {
    return id == entry.cca3;
  });
  if (!temp.length) {
    return (
      <>
        <Loader />
      </>
    );
  }

  let tempArr = Object.values(temp[0].languages);

  return (
    <div className={(dark && "countrydiv cstmblack") || "countrydiv"}>
      <Link to={"/"}>
        <div className="backbtndiv">
          <button className={dark && ' gap cstblack' || 'gap'}>
            <i className="fa-solid fa-arrow-left"></i>
            Back
          </button>
        </div>
      </Link>

      <div className="countrycard">
        <img src={temp[0].flags.svg} alt="" />

        <div className='countrydetails'>
          <h1>{temp[0].name.common}</h1>
          <div className="details">
            <div>
              <div>
                <b>Native Name:</b>{" "}
                {temp[0].name.nativeName
                  ? Object.values(temp[0].name.nativeName)[0].common
                  : "N/A"}
              </div>

              <div>
                <b>Population:</b> {temp[0].population}
              </div>
              <div>
                <b>Region: </b>
                {temp[0].region}
              </div>
              <div>
                <b>Sub Region:</b> {temp[0].subregion}
              </div>
              <div>
                <b>Capital:</b> {temp[0].capital}
              </div>
            </div>

            <div>
              <div>
                <b>Top Level Domain: </b> {temp[0].tld}
              </div>
              <div>
                <b>Currencies: </b>{" "}
                {temp[0].currencies
                  ? Object.keys(temp[0].currencies)[0]
                  : "N/A"}
              </div>

              <div>
                <b>Languages:</b>{" "}
                {tempArr.map((entry,index) => {
                  return <div key={index}>{entry} </div>;
                })}
              </div>
            </div>
          </div>
        <BorderCountries temp={temp}/>
        </div>
      </div>
    </div>
  );
};

export default SingleCountry;
