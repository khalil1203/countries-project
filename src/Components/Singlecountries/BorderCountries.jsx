import React, { useContext } from "react";
import './SingleCountry.css';
import { Link } from "react-router-dom";
import { DarkThemeContext } from "../../DarkThemeContext";

const BorderCountries = ({temp}) => {
    const {dark} = useContext(DarkThemeContext);
  return (
    <div >
      <div>
        <b>Border Countries:</b>
      </div>
      <div className="wrap">
        {temp[0].borders ? (
          temp[0].borders.map((code) => {
            return (
              <Link key={code} to={`/country/${code}`}>
                <button className={dark && 'borderbtn cstblack' || 'borderbtn'}>{`${code}`}</button>
              </Link>
            );
          })
        ) : (
          <p>N/A</p>
        )}
      </div>
    </div>
  );
};

export default BorderCountries;
