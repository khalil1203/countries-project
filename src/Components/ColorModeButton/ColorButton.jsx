import React, { useContext, useState } from 'react'
import { DarkThemeContext } from '../../DarkThemeContext'

const ColorButton = () => {
const {dark,setDark} = useContext(DarkThemeContext);
const [mode,setMode] = useState("Dark");

function changeColor(){
    setDark(!dark);
    if(mode == "Dark"){
        setMode("White")
    }
    else{
        setMode("Dark");
    }
}

  return (
      <button className={dark && 'colorbtndiv black' || 'colorbtndiv'} onClick={changeColor}>{mode} mode</button>
  )
}

export default ColorButton
