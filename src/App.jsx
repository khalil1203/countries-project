import { createBrowserRouter } from "react-router-dom";
import Home from "./Components/Home";
import "./App.css";
import { useEffect, useState } from "react";
import { Routes, Route } from "react-router-dom";
import Header from "./Components/Header/Header";
import SingleCountry from "./Components/Singlecountries/SingleCountry.jsx";
import Error from "./Components/Error/Error";


function App() {
  const [custom, setCustom] = useState([]);
  const [countries, setCountries] = useState([]);
  const [currentData, setCurrentData] = useState([]);
  const [sortedData, setSortedData] = useState([]);
  const [loader, setLoader] = useState(true);
  const [error,setError] = useState(false);

  useEffect(() => {
    setLoader(true);
    fetch("https://restcountries.com/v3.1/all")
      .then((data) => data.json())
      .then((data) => {
        setCustom(data);
        setCountries(data);
        setCurrentData(data);
        setLoader(false);
      }).catch((err) => {
        console.log(err, "my error");
        setError(true);
      })
      .finally(() => {
        if(error){
          setLoader(false); 
        }
      });

  }, []);

  if(error){
    return(
      <>
      <Header/>
      <Error/>
      </>
    )
  }
  else{

    return (
      <>
        <Routes>
          <Route
            path="/"
            element={
              <>
                <Home
                  custom={custom}
                  countries={countries}
                  setCustom={setCustom}
                  currentData={currentData}
                  setCurrentData={setCurrentData}
                  sortedData={sortedData}
                  setSortedData={setSortedData}
                  loader={loader}
                />
              </>
            }
          ></Route>
  
          <Route
            path="/country/:id"
            element={
              <>
                <Header />
                <SingleCountry countries={countries} />
              </>
            }
          ></Route>
        </Routes>
      </>
    );
  }

}

export default App;
