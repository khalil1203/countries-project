import { createContext, useState, useContext } from "react";

export const DarkThemeContext = createContext();

export default function DarkThemeProvider({ children }) {
  const [dark, setDark] = useState(false);

  return (
    <DarkThemeContext.Provider value={{dark,setDark}}>
        {children}
    </DarkThemeContext.Provider>
  );
}