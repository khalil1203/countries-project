import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import DarkThemeProvider from './DarkThemeContext.jsx'
import { BrowserRouter } from 'react-router-dom'

ReactDOM.createRoot(document.getElementById('root')).render(
  <BrowserRouter>
  <DarkThemeProvider>
    <App />
  </DarkThemeProvider>
  </BrowserRouter>
)
